package main

import (
	"fmt"
	"github.com/fogleman/gg"
	"gitlab.com/rege/tlcache"
	"gitlab.com/rege/vgenerator"
	"log"
	"time"
)

const (
	ImageWidth  = 2048
	ImageHeight = 1024
)

func main() {
	cache, err := tlcache.New(time.Second, time.Second/10, 1000, "cache")
	if err != nil {
		log.Fatal("failed to init the cache:", err)
	}
	defer cache.Flush()

	dc := gg.NewContext(ImageWidth, ImageHeight)
	dc.SetRGB(1, 1, 1)
	dc.Clear()
	dc.SetLineWidth(3)

	type point struct {
		x, y float64
	}
	lastPointOfCountInRAM := point{0, 0}
	lastPointOfCountOnDisk := point{0, 0}
	for i := 0; i < 512; i++ {
		for j := 0; j < 4; j++ {
			cache.PutForever(fmt.Sprintf("item%d.%d", i, j), vgenerator.GenRandomValue())
		}
		{ // RAM cache
			newPointOfCountInRAM := point{float64(i * 4), float64(cache.CountInRAM()) / 2}
			dc.SetRGB(1, 0, 0)
			dc.DrawLine(lastPointOfCountInRAM.x, ImageHeight-lastPointOfCountInRAM.y,
				newPointOfCountInRAM.x, ImageHeight-newPointOfCountInRAM.y)
			dc.Stroke()
			lastPointOfCountInRAM = newPointOfCountInRAM
		}
		{ // disk cache
			newPointOfCountOnDisk := point{float64(i * 4), float64(cache.CountOnDisk()) / 2}

			dc.SetRGB(0, 1, 0)
			dc.DrawLine(lastPointOfCountOnDisk.x, ImageHeight-lastPointOfCountOnDisk.y,
				newPointOfCountOnDisk.x, ImageHeight-newPointOfCountOnDisk.y)
			dc.Stroke()
			lastPointOfCountOnDisk = newPointOfCountOnDisk
		}

		time.Sleep(time.Millisecond)
	}

	dc.SetLineWidth(3)
	dc.SetRGB(0, 0, 0)
	dc.SetLineWidth(3)
	dc.DrawLine(0, ImageHeight-500, ImageWidth, ImageHeight-500)
	dc.Stroke()

	dc.Scale(3, 3)
	dc.SetRGB(1, 0, 0)
	dc.DrawString("Count of items in RAM", 10, 15)
	dc.SetRGB(0, 1, 0)
	dc.DrawString("Count of items on Disk", 10, 30)
	dc.SetRGB(0, 0, 0)
	dc.DrawString("Threshold", 10, 45)

	err = dc.SavePNG("plot.png")
	if err != nil {
		log.Fatal(err)
	}
}
